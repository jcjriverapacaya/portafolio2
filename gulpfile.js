
    var gulp = require('gulp');
    var browserSync= require('browser-sync').create();
    var autoprefixer = require('gulp-autoprefixer');
    var sass = require('gulp-sass');
    var concat = require('gulp-concat');
    var imagemin = require('gulp-imagemin');
    var uglify = require('gulp-uglify-es').default;
    var rename = require('gulp-rename');
    var cleanCSS = require('gulp-clean-css');
    var del = require('del');
    
    var paths = {
      css:{
        src:'src/css/**/*.css',
        dest:'src/assets/css'
      },
      scss: {
        src: 'src/scss/**/*.scss',
        dest: 'src/assets/css/'
      },
      scripts: {
        src: 'src/js/**/*.js',
        dest: 'src/assets/js/'
      },
      pages:{
        src:'src/pages/**/*.html',
        dest:'src/assets/'
      },
      media: {
        src: 'src/media/**/*.*',
        dest:'src/assets/media/'
      }
    };

    function clean() {
      return del([ 'assets' ]);
    }

    function pages(){
      return gulp.src(paths.pages.src)
        .pipe(gulp.dest(paths.pages.dest));
    }
    function css(){
      return gulp.src(paths.css.src)
        .pipe(autoprefixer('last 2 versions'))
        .pipe(cleanCSS())
        .pipe(rename({
          suffix: '.min'
        }))
        .pipe(gulp.dest(paths.css.dest));
    }
    function scss() {
      return gulp.src(paths.scss.src)
        .pipe(sass())
        .pipe(autoprefixer('last 2 versions'))
        .pipe(cleanCSS())
        .pipe(rename({
          basename: 'styles',
          suffix: '.min'
        }))
        .pipe(gulp.dest(paths.scss.dest));
    }
    function images(){
      return gulp.src(paths.media.src)
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest(paths.media.dest));
    }
    function scripts() {
      return gulp.src(paths.scripts.src, { sourcemaps: true })
        .pipe(rename({
          suffix: '.min'
        }))
        .pipe(uglify())
        .pipe(concat('all.min.js'))
        .pipe(gulp.dest(paths.scripts.dest));
    }
    
    function watch() {
      browserSync.init({
        server: {
          baseDir: "./src/assets/"
        }
      });
      gulp.watch(paths.pages.src,pages);
      gulp.watch(paths.css.src,css);
      gulp.watch(paths.scripts.src, scripts);
      gulp.watch(paths.scss.src, scss);
      gulp.watch(paths.media.src, images) ;
      gulp.watch([paths.pages.dest, paths.scripts.dest ,paths.css.dest, paths.scss.dest, paths.media.dest]).on('change',browserSync.reload);
    }
    
   exports.clean = clean;
   exports.pages = pages;
   exports.css = css;
   exports.scss = scss;
   exports.images = images;
   exports.scripts = scripts;
   exports.watch = watch;
  
  
  var build = gulp.series(clean, gulp.parallel(pages,css,scss,images, scripts,watch));
  gulp.task('default',build);