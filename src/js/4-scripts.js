// animacion scroll
const scroll = new SmoothScroll('a[href*="#"]' , {
	speed: 500,
	speedAsDuration: true
})

// activar menu mediante scroll

const navChange = document.getElementById("header")

window.addEventListener("scroll", () => {
  if (window.scrollY >= 1) {
    navChange.classList.add("header-active")
  } else {
    navChange.classList.remove("header-active")
  }
});

// dar funcionalidad al boton hamburguesa mediante click
const burgerBtn = document.getElementById("burgerBtn")
const nav = document.getElementById("nav")
const burgerICon = document.getElementById("burgerIcon")
const links = Array.from(document.querySelectorAll('.nav-link'))

burgerBtn.addEventListener("click", () => {
  nav.classList.toggle("nav-active");
  burgerICon.classList.toggle("fa-times");
});

//quitar menu hamburguesa al seleccionar un link

for (i = 0 ; i<links.length;i++) {
    links[i].addEventListener('click', () => {
      nav.classList.toggle('nav-active')
      burgerICon.classList.toggle("fa-times");
    })
}

// animacion a los skills

const bar1 = document.getElementById("bar1"),
      bar2 = document.getElementById("bar2"),
      bar3 = document.getElementById("bar3")

function animateBar(e, mw) {
  const smallBP = matchMedia("(max-width:991px)");
  const desktopBP = matchMedia("(min-width:992px)");
  if (scrollY >= 816 && smallBP.matches) {
    e.style.animation = "animateBar 3s ease";
    e.style.maxWidth = mw;
  } else if (scrollY >= 372 && desktopBP.matches) {
    e.style.animation = "animateBar 3s ease";
    e.style.maxWidth = mw;
  } else {
    e.style.animation = "none";
    e.style.maxWidth = mw;
  }
}

addEventListener("scroll", () => animateBar(bar1, bar1.textContent));
addEventListener("scroll", () => animateBar(bar2, bar2.textContent));
addEventListener("scroll", () => animateBar(bar3, bar3.textContent));


